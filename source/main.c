/*
 * This is a starting point for the thinventions programming task
 *
 *	Datasheet for the FRDM-KL27z board: http://www.mouser.com/pdfdocs/FRDMKL27ZUG.pdf
 *	Datasheet for the MMA8451Q accelerometer: http://www.nxp.com/assets/documents/data/en/data-sheets/MMA8451Q.pdf
 *
 * rick@thinventions.de, 2016
 */

#include <stdlib.h>
#include "fsl_debug_console.h"
#include "board.h"
#include "fsl_mma.h"
#include "pin_mux.h"
#include "fsl_port.h"

/*******************************************************************************
 * Definitions
 ******************************************************************************/

#define ACCEL_ADDRESS 0x1D

/*******************************************************************************
 * Variables
 ******************************************************************************/
i2c_master_handle_t g_MasterHandle;

/*******************************************************************************
 * Code
 ******************************************************************************/

int main(void)
{
    mma_handle_t mmaHandle = {0};
    mma_data_t sensorData = {0};
    i2c_master_config_t i2cConfig = {0};
    uint8_t sensorRange = 0x02;
    uint32_t i2cSourceClock = 0;
    int16_t xData = 0;
    int16_t yData = 0;
    int16_t zData = 0;
    uint8_t i = 0;
    uint8_t regResult = 0;
    uint8_t array_addr_size = 0;

    /* Board pin, clock, debug console init */
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_I2C_ConfigurePins();
    BOARD_InitDebugConsole();

    CLOCK_EnableClock(kCLOCK_PortA);
    CLOCK_EnableClock(kCLOCK_PortB);

    // Setup the LED pins
    PORT_SetPinMux(BOARD_LED_RED_GPIO_PORT, BOARD_LED_RED_GPIO_PIN, kPORT_MuxAsGpio);
   	PORT_SetPinMux(BOARD_LED_GREEN_GPIO_PORT, BOARD_LED_GREEN_GPIO_PIN, kPORT_MuxAsGpio);
    PORT_SetPinMux(BOARD_LED_BLUE_GPIO_PORT, BOARD_LED_BLUE_GPIO_PIN, kPORT_MuxAsGpio);
    LED_RED_INIT(LOGIC_LED_OFF);
    LED_GREEN_INIT(LOGIC_LED_OFF);
    LED_BLUE_INIT(LOGIC_LED_OFF);

    i2cSourceClock = CLOCK_GetFreq(ACCEL_I2C_CLK_SRC);
    mmaHandle.base = BOARD_ACCEL_I2C_BASEADDR;
    mmaHandle.i2cHandle = &g_MasterHandle;

    I2C_MasterGetDefaultConfig(&i2cConfig);
    I2C_MasterInit(BOARD_ACCEL_I2C_BASEADDR, &i2cConfig, i2cSourceClock);
    I2C_MasterTransferCreateHandle(BOARD_ACCEL_I2C_BASEADDR, &g_MasterHandle, NULL, NULL);

    /* Find sensor device */
    mmaHandle.xfer.slaveAddress = ACCEL_ADDRESS;
	if (MMA_ReadReg(&mmaHandle, kMMA8451_WHO_AM_I, &regResult) == kStatus_Success)
	{
		PRINTF("\r\nFound sensor device!\r\n");
	} else {
		PRINTF("\r\nSensor device not found\r\n");
		while (1)
		{
		};
	}

    if (MMA_WriteReg(&mmaHandle, kMMA8451_XYZ_DATA_CFG, sensorRange) != kStatus_Success)
    	return -1;

    /* Init accelerometer sensor */
    if (MMA_Init(&mmaHandle) != kStatus_Success)
    {
        return -1;
    }

    /* Main loop. Get sensor data and switch LED accordingly */
    while (1)
    {
        /* Get new accelerometer data. */
        if (MMA_ReadSensorData(&mmaHandle, &sensorData) != kStatus_Success)
        {
            return -1;
        }

        /* Get the X and Y data from the sensor data structure in 14 bit left format data*/
        xData = (int16_t)((uint16_t)((uint16_t)sensorData.accelXMSB << 8) | (uint16_t)sensorData.accelXLSB) / 4U;
        yData = (int16_t)((uint16_t)((uint16_t)sensorData.accelYMSB << 8) | (uint16_t)sensorData.accelYLSB) / 4U;
        zData = (int16_t)((uint16_t)((uint16_t)sensorData.accelZMSB << 8) | (uint16_t)sensorData.accelZLSB) / 4U;

        // Get the dominant Axis
        char dom_axis = 'z';
        if (abs(xData) > abs(yData) && abs(xData) > abs(zData)) {
        	 dom_axis = 'x';
        } else if (abs(yData) > abs(xData) && abs(yData) > abs(zData)) {
			 dom_axis = 'y';
		}

        switch (dom_axis) {
        	case 'x':
        		LED_RED_OFF();
				LED_BLUE_ON();
				LED_GREEN_OFF();
        		break;
        	case 'y':
        		LED_RED_ON();
				LED_BLUE_OFF();
				LED_GREEN_OFF();
        		break;
        	case 'z':
        		LED_RED_OFF();
				LED_BLUE_OFF();
				LED_GREEN_ON();
        		break;
        }

        /* Print out the dominant axis via serial bus (COMx, 115200 baud)  */
        PRINTF("Dominant axis: %c\r\n", dom_axis);
    }
}
